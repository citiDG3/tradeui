import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Citi' },
  { id: 12, name: 'APPL' },
  { id: 13, name: 'TSLA' },
  { id: 14, name: 'AMZN' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];
