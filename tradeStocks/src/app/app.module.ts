import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { ChartsComponent } from './charts/charts.component';
import { NgxEchartsModule } from 'ngx-echarts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TickerDetailsComponent } from './ticker-details/ticker-details.component';
import { TradeHistoryComponent } from './trade-history/trade-history.component';
import { TickerListComponent } from './ticker-list/ticker-list.component';
import { TickersComponent } from './tickers/tickers.component';

import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { enableRipple } from '@syncfusion/ej2-base';
//import { PiechartComponent } from './piechart/piechart.component';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';

enableRipple(true);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TickersComponent,
    TickerDetailsComponent,
    TradeHistoryComponent,
    TickerListComponent,
    ChartsComponent
   // PiechartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    ButtonModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    FormsModule,
    BrowserAnimationsModule,
    NgbModule    
    
  ],
  providers:[],
  bootstrap: [AppComponent]
})
export class AppModule { }


