import { Component, OnInit, Input } from '@angular/core';
import { TickerService } from '../ticker.service';

@Component({
  selector: 'app-tickers',
  templateUrl: './tickers.component.html',
  styleUrls: ['./tickers.component.css']
})
export class TickersComponent implements OnInit {
  // result = ''
  // buyDetails = [{ticker: 'APPL', price_data: ''}]
  tickerInput
  amountInput
  tickerDetailsResult: any
  price:number
  CurrentDate = new Date();

  data: any ={ created: "2020-02-17T34:55:52.423+00:00",
          amount: 10,
          requestedPrice: 900,
          state: '',
          type: "SELL"
        }

        // = [{price_data: ''}]

    // data: { id: "5f634eeb2fcb9e035085769b",
    // created: "2020-09-17T11:55:52.423+00:00",
    // ticker: "TSLA",
    // amount: 10,
    // requestedPrice: 900,
    // state: "FILLED",
    // type: "SELL"}

  constructor(private tickerService:TickerService) { }

  ngOnInit(): void {
  }

  
  getTickerDetails(){
    this.tickerService.getTicker().subscribe((data) => this.tickerDetailsResult= data)
    console.log('Button works')
  }

    
  getTickerPriceDetails(type){
    this.tickerService.getTickerPrice(this.tickerInput).subscribe((data) =>{ 
      this.tickerDetailsResult= data
      this.price = this.tickerDetailsResult.price_data[0][1]
     console.log(this.price)
     data['amount']=this.amountInput 
     data['requestedPrice']=this.price*this.amountInput
     data['type']=type
     data['state']="CREATED"
     data['created']=this.CurrentDate
      this.tickerService.doPOST(data).subscribe((response)=>{console.log(response)})
  }
 )
}
}
 