import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TickerCardsComponent } from './tickers.component';

describe('TickerCardsComponent', () => {
  let component: TickerCardsComponent;
  let fixture: ComponentFixture<TickerCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TickerCardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TickerCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
