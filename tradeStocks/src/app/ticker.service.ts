import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TickerService {
getAllURL = 'http://citieurlinux3.conygre.com:8080/trade/getAlltrade'


restURL = 'http://citieurlinux3.conygre.com:8080/trade/getAlltrade'
postURL = 'http://citieurlinux3.conygre.com:8080/trade'
yhfURL = 'https://2ajczm4s1f.execute-api.eu-west-1.amazonaws.com/default/dg3Function?ticker=AAPL&num_days=5'
yhfURLBase = 'https://2ajczm4s1f.execute-api.eu-west-1.amazonaws.com/default/dg3Function'


  constructor(private http:HttpClient) { 
  }
  getAll(){
    return this.http.get(this.getAllURL)
  }
  
  getTicker(){
    return this.http.get(this.yhfURL)
  }


  getTickerDyn(id, days){
    return this.http.get(this.yhfURLBase+"?ticker=" + id + "&num_days=" + days)
  }

  getTickerPrice(id){
    return this.http.get(this.yhfURLBase+"?ticker=" + id + "&num_days=1" )
  }


  doPOST(data){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      })
    }
    return this.http.post(this.postURL, data, httpOptions)
  } 

}
