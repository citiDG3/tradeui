import { Component, OnInit } from '@angular/core';
import { TickerService } from '../ticker.service';

@Component({
  selector: 'charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  options: any;
  tickerDetailsResult: any;
  tickerDetailsResult2: any;
  constructor(private tickerService: TickerService ) {}

  ngOnInit(): void {
    const xAxisData = [];
    const data1 = [];
    const data2 = [];



    this.tickerService.getTickerDyn("AAPL",30).subscribe((data) => {this.tickerDetailsResult= data
      

  
     for (let i = 0; i < this.tickerDetailsResult.price_data.length; i++) {
       xAxisData.push(this.tickerDetailsResult.price_data[i][0]);
       console.log("on" + this.tickerDetailsResult.price_data[i][1]);
       data1.push(this.tickerDetailsResult.price_data[i][1]);
     }

     this.tickerService.getTickerDyn("C",30).subscribe((data) => {this.tickerDetailsResult2= data
   
      for (let i = 0; i < this.tickerDetailsResult2.price_data.length; i++) {
        console.log(this.tickerDetailsResult2.price_data[i][1]);
        data2.push(this.tickerDetailsResult2.price_data[i][1]);
      }



    this.options = {
      legend: {
        data: ['Apple', 'Citi'],
        align: 'left',
      },
      tooltip: {},
      xAxis: {
        data: xAxisData,
        silent: false,
        splitLine: {
          show: false,
        },
      },
      yAxis: {},
      series: [
        {
          name: 'Apple',
          type: 'bar',
          data: data1,
          animationDelay: (idx) => idx * 10,
        }
        ,
        {
          name: 'Citi',
          type: 'bar',
          data: data2,
          animationDelay: (idx) => idx * 10 + 100,
        }
        // ,
        // {
        //   name: 'Stock3',
        //   type: 'bar',
        //   data: data3,
        //   animationDelay: (idx) => idx * 10 + 50,
        // },
      ],
      animationEasing: 'elasticOut',
      animationDelayUpdate: (idx) => idx * 5,
    };
 
})
})
}   

}