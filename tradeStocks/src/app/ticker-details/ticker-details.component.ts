import { Component, OnInit } from '@angular/core';
import { TickerService } from '../ticker.service';

@Component({
  selector: 'app-ticker-details',
  templateUrl: './ticker-details.component.html',
  styleUrls: ['./ticker-details.component.css']
})
export class TickerDetailsComponent implements OnInit {

tickerDetailsResult:any = {"ticker": "C", "price_data": [{"date": "2020-08-31", "price": 51.119998931884766}]}
  
  constructor(private tickerService: TickerService) { }

  ngOnInit(): void {
    this.getTickerDetails()
  }

  getTickerDetails(){
    this.tickerService.getTicker().subscribe((data) => this.tickerDetailsResult= data)
    
  }
  
}

