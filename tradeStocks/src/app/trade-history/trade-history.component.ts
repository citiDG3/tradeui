import { Component, OnInit } from '@angular/core';
import { TickerService } from '../ticker.service';

@Component({
  selector: 'app-trade-history',
  templateUrl: './trade-history.component.html',
  styleUrls: ['./trade-history.component.css']
})
export class TradeHistoryComponent implements OnInit {
result: any

  constructor(private tickerService: TickerService) { }

  ngOnInit(): void {
    this.getAllTrades()
  }

  getAllTrades(){
    this.tickerService.getAll().subscribe((data) => this.result= data)
  }

}
