import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TradeHistoryComponent } from './trade-history/trade-history.component';
import { TickerDetailsComponent } from './ticker-details/ticker-details.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
//import { TickerCardsComponent } from './tickers/tickers.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TickersComponent } from './tickers/tickers.component';
import { ChartsComponent } from './charts/charts.component';
//import { PiechartComponent } from './piechart/piechart.component';




const routes: Routes = [
  // here we declare the client-side routes for this app
  { path:'', component:DashboardComponent},
  { path:'trade-history', component:TradeHistoryComponent},
  { path:'tickers', component:TickersComponent},
  { path:'ticker-details', component:TickerDetailsComponent},
  { path:'dashboard', component:ChartsComponent},
  { path:'**', redirectTo:'dashboard' },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
